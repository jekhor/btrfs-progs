/*
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License v2 as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 021110-1307, USA.
 */

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/ioctl.h>
#include <errno.h>
#include <getopt.h>

#include "ctree.h"
#include "utils.h"
#include "volumes.h"
#include "disk-io.h"

#include "commands.h"

int print_spare_device(unsigned unit_mode)
{
	int ret;
	struct btrfs_fs_devices *fs_devices;
	struct btrfs_device *device;
	struct list_head *fs_uuids;

	printf("Global spare\n");

	ret = btrfs_scan_lblkid();
	if (ret) {
		fprintf(stderr, "scan_lblkid failed ret %d\n", ret);
		return ret;
	}

	fs_uuids = btrfs_scanned_uuids();

	list_for_each_entry(fs_devices, fs_uuids, list) {
		if (!fs_devices->spare)
			continue;

		device = list_entry(fs_devices->devices.next,
					struct btrfs_device, dev_list);
		if (device->name)
			printf("\tdevice size %s path %s\n",
				pretty_size_mode(device->total_bytes,
					unit_mode), device->name);

	}

	return 0;

}

static void btrfs_delete_spare(char *path)
{
	printf("Unscan the device (or don't run device scan after reboot) and run wipefs to wipe SB\n");

}

static void btrfs_add_spare(char *dev)
{
	struct stat st;
	int fd;
	int i;
	int ret;
	u64 block_cnt;
	u64 blocks[7];
	u32 nodesz = max_t(u32, sysconf(_SC_PAGESIZE), BTRFS_MKFS_DEFAULT_NODE_SIZE);
	struct btrfs_mkfs_config mkfs_cfg;

	fd = open(dev, O_RDWR);
	if (fd < 0) {
		fprintf(stderr, "unable to open %s: %s\n", dev, strerror(errno));
		return;
	}

	if (fstat(fd, &st)) {
		fprintf(stderr, "unable to stat %s\n", dev);
		goto out;
	}
	block_cnt = btrfs_device_size(fd, &st);
        if (!block_cnt) {
                fprintf(stderr, "unable to find %s size\n", dev);
		goto out;
        }

	if (block_cnt < BTRFS_MKFS_SYSTEM_GROUP_SIZE) {
		fprintf(stderr, "device is too small to make filesystem\n");
		goto out;
	}

	blocks[0] = BTRFS_SUPER_INFO_OFFSET;
	for (i = 1; i < 7; i++)
		blocks[i] = BTRFS_SUPER_INFO_OFFSET + 1024 * 1024 + nodesz * i;

	memset(&mkfs_cfg, 0, sizeof(mkfs_cfg));
	memcpy(mkfs_cfg.blocks, blocks, sizeof(blocks));
	mkfs_cfg.num_bytes = block_cnt;
	mkfs_cfg.nodesize = nodesz;
	mkfs_cfg.sectorsize = 4096;
	mkfs_cfg.stripesize = 4096;
	mkfs_cfg.features = BTRFS_FEATURE_INCOMPAT_SPARE_DEV;
	ret = make_btrfs(fd, &mkfs_cfg);
	if (ret)
		fprintf(stderr, "error during mkfs: %s\n", strerror(-ret));

out:
	close(fd);
}

static const char * const spare_cmd_group_usage[] = {
	"btrfs spare <command> [<args>]",
	NULL
};

static const char * const cmd_spare_add_usage[] = {
	"btrfs spare add <device> [<device>...]",
	"Add global spare device(s) to btrfs",
	"-K|--nodiscard    do not perform whole device TRIM",
	"-f|--force        force overwrite existing filesystem on the disk",
	NULL
};

static const char * const cmd_spare_delete_usage[] = {
	"btrfs spare delete <device> [<device>...]",
	"Delete global spare device(s) from btrfs",
	NULL
};

static const char * const cmd_spare_list_usage[] = {
	"btrfs spare list",
	"List spare device(s) both scanned and unscanned(*) for kernel",
	NULL
};

static int cmd_spare_add(int argc, char **argv)
{
	int i;
	int force = 0;
	int discard = 1;
	int ret = 0;

	while (1) {
		int c;
		static const struct option long_options[] = {
			{ "nodiscard", optional_argument, NULL, 'K'},
			{ "force", no_argument, NULL, 'f'},
			{ NULL, 0, NULL, 0}
		};

		c = getopt_long(argc, argv, "f", long_options, NULL);
		if (c < 0)
			break;

		switch (c) {
		case 'K':
			discard = 0;
			break;
		case 'f':
			force = 1;
			break;
		default:
			usage(cmd_spare_add_usage);
		}
	}

	if (check_argc_min(argc - optind, 1))
		usage(cmd_spare_add_usage);

	for (i = optind; i < argc; i++) {
		u64 dev_block_count = 0;
		int devfd;
		char *path;
		int res;

		if (test_dev_for_mkfs(argv[i], force)) {
			ret++;
			continue;
		}

		devfd = open(argv[i], O_RDWR);
		if (devfd < 0) {
			fprintf(stderr, "ERROR: Unable to open device '%s'\n", argv[i]);
			ret++;
			continue;
		}

		res = btrfs_prepare_device(devfd, argv[i], 1, &dev_block_count,
								0, discard);
		close(devfd);
		if (res) {
			ret++;
			goto error_out;
		}

		path = canonicalize_path(argv[i]);
		if (!path) {
			fprintf(stderr,
				"ERROR: Could not canonicalize pathname '%s': %s\n",
				argv[i], strerror(errno));
			ret++;
			goto error_out;
		}

		btrfs_add_spare(path);
		free(path);
	}
error_out:
	btrfs_close_all_devices();
	return !!ret;
}

static int cmd_spare_delete(int argc, char **argv)
{
	int i;
	char *path;
	int ret = 0;

	if (check_argc_min(argc - optind, 1))
		usage(cmd_spare_add_usage);

	for (i = optind; i < argc; i++) {
		int devfd;

		devfd = open(argv[i], O_RDWR);
		if (devfd < 0) {
			fprintf(stderr, "ERROR: Unable to open device '%s'\n", argv[i]);
			ret++;
			continue;
		}
		close(devfd);

		path = canonicalize_path(argv[i]);
		if (!path) {
			fprintf(stderr,
				"ERROR: Could not canonicalize pathname '%s': %s\n",
				argv[i], strerror(errno));
			ret++;
			goto error_out;
		}

		btrfs_delete_spare(path);
		free(path);
	}

error_out:
	btrfs_close_all_devices();
	return !!ret;
}

int cmd_spare_list(int argc, char **argv)
{
	int ret;
	unsigned unit_mode;

	unit_mode = get_unit_mode_from_arg(&argc, argv, 0);

	ret = print_spare_device(unit_mode);

	return !!ret;
}

static const char spare_cmd_group_info[] =
	"manage spare devices in the filesystem";

const struct cmd_group spare_cmd_group = {
	spare_cmd_group_usage, spare_cmd_group_info, {
		{ "add", cmd_spare_add, cmd_spare_add_usage, NULL, 0 },
		{ "delete", cmd_spare_delete, cmd_spare_delete_usage, NULL, 0},
		{ "list", cmd_spare_list, cmd_spare_list_usage, NULL, 0},
		NULL_CMD_STRUCT
	}
};

int cmd_spare(int argc, char **argv)
{
	return handle_command_group(&spare_cmd_group, argc, argv);
}
